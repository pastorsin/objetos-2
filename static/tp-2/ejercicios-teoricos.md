## Ejercicio 3:

1. Falso, no cambia el comportamiento. Por cada refactoring hay que volver a correr los tests para comprobar que el comportamiento es el mismo.
2. Falso, si se refactoriza hay que testear siempre para comprobar que tiene el mismo comportamiento.
3. Falso, la estructura interna del código se modifica con más métodos privados por ejemplo o con métodos delegados hacia otras clases, etc.
4. Verdadero, la refactorización debe ser realizada como un paso separado, para poder comprobar con mayor facilidad que no se han introducido errores al llevarla a cabo.

## Ejercicio 4:

1. Signfica que el código presenta indicios de problemas que requieren la aplicación de refactorings.
2. ....
3. Basicamente permite mejor mantenimiento:
3.1. Permite entender mejor el código
3.2. Las modificaciones y agregaciones tardan menos
3.3. Diseño limpio y bien organizado

## Ejercicio 5

Patrones ???

## Ejercicio 6

1. ### Malos olores:
    * **Long method**: Podría ser más corto, el método hace más de una cosa.
    * **Duplicated code:** Tanto *tweetCountByUser* como *tweetCountByUserGender* repiten la contabilización del usuario.
    * **Temporary field:** result.
    * **No reinventar la rueda:**  Se está armando con el diccionario un bag.
    
2. ### Tests:

    * Caso donde devuelve vacío (Vale para los 2 métodos) --> Si no hay tweets.
    * Caso donde devuelve 1 (Vale para los 2 métodos). --> Prueba que registra al menos 1
    * Caso donde devuelve más de 1 (Vale para los 2 métodos). --> Prueba que contabiliza más de 1

3. ### Refactoring:

```python
TweetBase>> tweetCountByUser
    ^ self tweetUsers valuesAndCount

TweetBase>> tweetUsers
    ^ tweets collect: [:tweet | tweet user] as: Bag

TweetBase>> tweetCountByUserGender
	^ self tweetUsersGender valuesAndCount

TweetBase>> tweetUsersGender
    ^ tweets collect: [:tweet | tweet userGender] as: Bag
        
Tweet>> userGender
	^ user gender
```




