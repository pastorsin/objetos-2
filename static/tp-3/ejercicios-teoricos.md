# TP 3 - Adapter, Template y Composite 

#### Ejercicio 1

1. Son los patrones de diseño software que solucionan problemas de composición (agregación) de clases y objetos.
2.  Hay una forma que lo implementa mediante herencia múltiple pero Smalltalk no permite hacer herencia múltiple.![1557788262165](/home/andres/.config/Typora/typora-user-images/1557788262165.png)

------

#### Ejercicio 2

1. Lo resuelve el patrón Adapter
2. ![1556997656255](/home/andres/.config/Typora/typora-user-images/1556997656255.png)
3. Implementación en package Objectos2-MediaPlayer en objetos-2.image

------

#### Ejercicio 3

* UML Class antes:

  ![1557002915384](/home/andres/.config/Typora/typora-user-images/1557002915384.png)

* UML Class después:![1557006357917](/home/andres/.config/Typora/typora-user-images/1557006357917.png)

* Implementación en package Biblioteca en objetos-2.image

------

#### Ejercicio 4

1. 
   * El esqueleto del algoritmo se define en una operación, deja que las subclases redefinan algunos pasos del algoritmo sin modificar la estructura del mismo.
   * No se puede redefinir el esqueleto.
   * Se puede redefinir algunos pasos del algoritmo que posee el esqueleto.
   * Hook method se le llama al método que redefine ese "paso" (step) del algoritmo en la subclase.
2. 
   * La clase Magnitude usa el patrón para las comparaciones, deja como hooks methods las comparaciones **< > <= >= =**.
   * La clase Collection usa el patrón para el #do
3. .
   * La clase Collection lo hace con el #add dejandolo como hook
   * La clase WATagCanvas lo hace con #space
4. ![1557015112706](/home/andres/.config/Typora/typora-user-images/1557015112706.png)

------

#### Ejercicio 5

![1557024298889](/home/andres/.config/Typora/typora-user-images/1557024298889.png)

*implementado en el package Objetos2-Topografias de objetos-2.image*

------

#### Ejercicio 6

![1557777239738](/home/andres/.config/Typora/typora-user-images/1557777239738.png)

*implementado en el package O2-FileSystem de objetos-2.image*

------

#### Ejercicio 7

Apliqué un Strategy para el Impresor y luego un Template dentro de Impresor.

*implementado en el package Objetos2-Topografias de objetos-2.image*

------

#### Ejercicio 8

Se puede aplicar un template para armar los sueldos mediante la suma de bonificaciones, sueldo basico y por hora. Ademas se descuenta para todos los empleados desde la clase abstracta