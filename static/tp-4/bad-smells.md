## Problemas (Bad-Smells) y Solución (Refactoring)

* Problema:
  * Las clases **Answer** y **Question** tienen código duplicado entre sí.
* Solución:
  * *Extract Superclass y Pull-Up:* Generar una herencia **Publication** que unifique el código duplicado entre ellas. Además "Answer is a Publication and Question is a Publication".

------

- Problema:
  - Los métodos **negativeVotes** y **positiveVotes** están reinventando la rueda, ya que están duplicando el comportamiento de *>OrderedCollection#select* en el caso de **positiveVotes** y *>OrderedCollection#reject* en el caso de **negativeVotes**.
- Solución
  - Usar los mensajes *select* y *reject* pertenecientes a la clase *OrderedCollection* respectivamente

------

* Problema:
  * El método **retrieveQuestions** 
    * Presenta un código gigante
    * Hace mucho y delega poco ya que trabaja con colecciones que no le pertenecen
    * Posee código duplicado
    * Switch statements
* Solución:
  * *Extract method to component*: Extraer el comportamiento perteneciente a las clases que poseen los atributos.
    * Se extrae el cuerpo del *if* de la opcion #social al método followingQuestions de la clase **User **
    * Se extrae el cuerpo del *if* de la opcion #topics al método questionsOfTopics de la clase **User **
    * Se extrae el cuerpo del *if* de la opcion #news al método newQuestions de la clase **CuOOra**
    * Se extrae el cuerpo del *if* de la opcion #popularToday al método todayPopularQuestions de la clase **CuOOra**
  * *Replace conditional with Polymorphism:* Se subclasifica **QuestionRetriever** en **NewsQuestionRetriever**, **PopularTodayQuestionRetriever**, **SocialQuestionRetriever**, **TopicsQuestionRetriever**

------

* Problema:
  * Los métodos **questionsOfTopics** y **followingQuestions**:
    * Reinventado la rueda » Se está haciendo un do por las preguntas de los topicos y followings respectivamente
  * Solución:
    * Usar el método flatCollect de OrderedCollection

------

- Problema:
  - El método **newQuestions **:
    - Reinventando la rueda » Se está haciendo un do por las preguntas de hoy
    - Envidia de atributos » Se le pide a la pregunta la fecha y la compara
- Solución
  - Usar el método select de OrderedCollection
  - *Extract method to component*: Delegar la comparación de la fecha a la Pregunta mediante el método **creationIsDay**

------

- Problema:

  - El método **todayPopularQuestions**: 
    - Posee código duplicado que hace lo mismo que el método **newQuestions .**
    - Variable temporal » averageVotes
    - Envidia de atributos » positiveVotes size
    - Solución:

  - Reusar el método **newQuestions**
  - *Replace Temp with Query:* Se elimina la variable del promedio mediante una query al average de la pregunta
  - *Extract method to component*: La cantidad de votos se delega a un método amountPositiveVotes en la clase Question

------

* Problema:
  * Los métodos **retrieveQuestions** poseen código duplicado ya que cada uno duplica la ordenación, tomar las primeras 100 preguntas y el reject del usuario. Además poseen variables temporales.
* Solución:
  * *Pull up:* Extraer el método de ordenación de las últimas 100 preguntas a la clase padre **QuestionRetriever**.
  * *Pull up*: Extrar el método de reject a la clase padre **QuestionRetriever**
  * *Replace Temp with Query:*  Eliminar las variables temporales y retornar el resultado de la colección mediante métodos anidados. 
    - Además en el nuevo método **lastQuestionsOrderByVotes**
      - *Replace Temp with Query:* Se eliminan las variables temporales mediante métodos de colección (takeFirst y sorted).
      - *Extract method to component*: Se delega la cantidad de votos en las publicaciones

------

* Problema:
  * El método **isTopAnswer** presenta envidia de atributos
* Solución:
  * *Extract method to component:* Se delega la cantidad de votos y el promedio a la pregunta.

------

* Problema:
  * Los métodos de inicialización de todas las clases están dispersos.
* Solución:
  * Definir un método de inicialización completo

