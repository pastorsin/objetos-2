# Práctica 5

## Ejercicio 1

![1558548938518](./images/1558548938518.png)

## Ejercicio 2

![1558549016824](./images/1558549016824.png)

## Ejercicio 3

![1558549082141](./images/1558549082141.png)

## Ejercicio 4

1. **¿Cuándo es necesario usar el patrón State?**

   1. En determinadas ocasiones, cuando el contexto en el que se está desarrollando requiere que un objeto tenga diferentes comportamientos según el estado en que se encuentra, resulta complicado poder manejar el cambio de comportamientos y los estados de dicho objeto, todos dentro del mismo bloque de código. El patrón State propone una solución a esta complicación, creando básicamente, un objeto por cada estado posible del objeto que lo llama.

2. **¿Qué representa el contexto dentro del patrón State?**

   1. **Context(Contexto):** Este integrante define la interfaz con el cliente. Mantiene una instancia de ConcreteState (Estado Concreto) que define su estado actual

      **State (Estado):**Define una interfaz para el encapsulamiento de la responsabilidades asociadas con un estado particular de Context.

      **Subclase ConcreteState:**Cada una de estas subclases implementa el comportamiento o responsabilidad de Context.

3. **¿Puede el estado concreto acceder al contexto?**.

   1. Sí, sin violar el encapsulamiento. La idea es que se delegue todo en el estado y luego este haga querys que necesite al contexto, pero sin violar el encapsulamiento. Una opción es pasarlo por parámetro y otra opción es conocimiento mutuo.

4. **¿Quién define las transiciones entre estados?**

   1. Las clases concretas deciden los cambios de estados (Es decir, cada estado cambia su estado). Esto permite una mayor cohesión del estado. Basicamente la clase Context no se entera de nada.

## Ejercicio 5

![1559088866882](./images/1559088866882.png)